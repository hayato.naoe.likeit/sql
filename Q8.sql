select 
	item.item_id,
	item.item_name,
	item.Item_price,
	item.category_id,
	item_category.category_name
from
	item
inner join
	item_category
on
	item.category_id = item_category.category_id;
