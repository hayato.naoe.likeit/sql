#Q1---------------
create table item_category(
	category_id int primary key NOT NULL AUTO_INCREMENT,
	category_name VARCHAR(256) NOT NULL
);

#Q2----------------
create table item(
	item_id int primary key NOT NULL AUTO_INCREMENT,
	item_name VARCHAR(256) NOT NULL,
	Item_price int NOT NULL,
	category_id int
);

#Q3----------------
insert into item_category(category_id,category_name) values (1,'家具');
insert into item_category(category_id,category_name) values (2,'食品');
insert into item_category(category_id,category_name) values (3,'本');

#Q4----------------
insert into item(item_id,item_name,item_price,category_id) values (1,'堅牢な机',3000,1);
insert into item(item_id,item_name,item_price,category_id) values (2,'生焼けの肉',50,2);
insert into item(item_id,item_name,item_price,category_id) values (3,'すっきりわかるJava入門',3000,3);
insert into item(item_id,item_name,item_price,category_id) values (4,'おしゃれな椅子',2000,1);
insert into item(item_id,item_name,item_price,category_id) values (5,'こんがり肉',500,2);
insert into item(item_id,item_name,item_price,category_id) values (6,'書き方ドリルSQL',2500,3);
insert into item(item_id,item_name,item_price,category_id) values (7,'ふわふわのベッド',30000,1);
insert into item(item_id,item_name,item_price,category_id) values (8,'ミラノ風ドリア',300,2);

#Q5-----------------
select item_name,item_price from item where category_id = 1;

#Q6
select item_name,item_price from item where item_price >= 1000;

#Q7-----------------
select item_name,item_price from item where item_name like '%肉%';

#Q8-----------------
select 
	item.item_id,
	item.item_name,
	item.Item_price,
	item.category_id,
	item_category.category_name
from
	item
inner join
	item_category
on
	item.category_id = item_category.category_id;

	
	
#Q9-----------------
select
    category_name,
    sum(Item_price) AS total_price
from
    item
inner join 
    item_category
on
    item.category_id = item_category.category_id
group by
    category_name;