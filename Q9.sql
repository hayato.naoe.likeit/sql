select
    category_name,
    sum(Item_price) AS total_price
from
    item
inner join 
    item_category
on
    item.category_id = item_category.category_id
group by
    category_name;
