// create database usermanagement DEFAULT CHARACTER SET utf8;

// create table user (
	id serial primary key not null UNIQUE AUTO_INCREMENT,
	login_id varchar(255) not null UNIQUE,
	name varchar(255) not null,
	birth_date date not null,
	password varchar(255) not null,
	create_date datetime not null,
	update_date datetime not null
	);
	
insert into user(id,login_id,name) values (1,'admin','管理者');